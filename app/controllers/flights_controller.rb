class FlightsController < ApplicationController
  def index
    
    @airport_options = Airport.all.map{|a| a.code}.uniq
    @dates = Flight.all.map{|f| f.depart_time.to_date}.uniq
    
    @from_airport = params[:from]
    @to_airport = params[:to]
    @date = params[:date]
    @passenger_num = params[:number]
    
    @flights = Flight.search(@from_airport, @to_airport, @date)
    
  end
  
end
