class BookingsController < ApplicationController
  def new
    @booking = Booking.new()
    @flight = Flight.find(params[:flight_option])
    @passenger_num = params[:passenger_num].to_i
    @passenger_num.times do 
      @booking.passengers.build
    end
  end
  
  def create
    @booking = Booking.create(booking_params)
    @booking.flight = Flight.find(params[:booking][:flight_id])
    if @booking.save
      PassengerMailer.thank_you(params[:booking][:passengers_attributes]['0']).deliver_now
      redirect_to booking_path @booking
    else
      render 'new'
    end
  end
  
  def show
    @booking = Booking.find(params[:id])
    @flight = @booking.flight
    @passengers = @booking.passengers
  end
  
  private
  def booking_params
    params.require(:booking).permit(:passengers_attributes => [:name, :email])
  end
end
