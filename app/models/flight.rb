class Flight < ActiveRecord::Base
    belongs_to :to_airport, class_name: 'Airport'
    belongs_to :from_airport, class_name: 'Airport'
    has_many :bookings
    
    def self.search(from, to, date)
        Flight.where(from_airport: Airport.find_by(code: from), to_airport: Airport.find_by(code: to), depart_time: Flight.correct_date(date)).order(:depart_time)
    end
    
    def self.correct_date(date)
        if !date.nil? 
            date = date.to_date 
            date = date.beginning_of_day..date.end_of_day
        end
    end
end
