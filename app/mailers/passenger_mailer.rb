class PassengerMailer < ApplicationMailer
    def thank_you(user)
       @user = user
       @url = 'https://demo-project-rugia.c9.io/'
       mail(to: @user[:email], subject: 'Thank you for ordering!') if !@user[:email].empty?
    end
end
