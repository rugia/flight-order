# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

tpe = Airport.create(code: 'TPE')
khs = Airport.create(code: 'KHS')

Flight.create([{depart_time: DateTime.new(2016,03,21,13,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])
Flight.create([{depart_time: DateTime.new(2016,03,21,12,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])
Flight.create([{depart_time: DateTime.new(2016,03,21,11,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])
Flight.create([{depart_time: DateTime.new(2016,03,21,10,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])

Flight.create([{depart_time: DateTime.new(2016,03,22,13,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])
Flight.create([{depart_time: DateTime.new(2016,03,22,12,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])
Flight.create([{depart_time: DateTime.new(2016,03,22,11,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])
Flight.create([{depart_time: DateTime.new(2016,03,22,10,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])

Flight.create([{depart_time: DateTime.new(2016,03,23,13,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])
Flight.create([{depart_time: DateTime.new(2016,03,23,12,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])
Flight.create([{depart_time: DateTime.new(2016,03,23,11,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])
Flight.create([{depart_time: DateTime.new(2016,03,23,10,30,00), duration: 1, from_airport_id: 1, to_airport_id: 2}])

Flight.create([{depart_time: DateTime.new(2016,03,24,13,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])
Flight.create([{depart_time: DateTime.new(2016,03,24,12,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])
Flight.create([{depart_time: DateTime.new(2016,03,24,11,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])
Flight.create([{depart_time: DateTime.new(2016,03,24,10,30,00), duration: 1, from_airport_id: 2, to_airport_id: 1}])