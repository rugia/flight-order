# Preview all emails at http://localhost:3000/rails/mailers/passenger_mailer
class PassengerMailerPreview < ActionMailer::Preview
    def thank_you
        PassengerMailer.thank_you(Passenger.find_by(name:'rugia'))
    end
end
